export enum FixedIds {
  SEARCHFIELDID = "SFId",
  SHAREWITHUSER = "SWUId",
  SHAREWITHLINK = "SWLId",
  INVITATION = "IID",
}

export enum ErrorMessages {
  DEFAULTERROR = "Something went wrong",
  FIELDSNOTSELECTED = "Please select all that is required ",
}
export enum ButtonTitles {
  YES = "Yes",
  NO = "No",
}

export enum PopupMesages {
  CONFIRMATION = "Are you sure?",
}
