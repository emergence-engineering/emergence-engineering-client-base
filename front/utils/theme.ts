export default {
  color: {
    primary: "#4488EE",
    secondary: "#ffffff",
    error: "#EE3311",
    inputBackground: "#CCCCAA",
    shadow: "#222222",
    button: "#E5E5E5",
    border: "#E2E2E2",
    gray0: "#FBFBFB",
    gray1: "#E2E2E2",
    gray2: "#C7C7C7",
    gray3: "#C4C4C4",
    gray4: "#B9B9B9",
    gray5: "#757575",
    gray6: "#5F5F5F",
    gray7: "#373737",
    white: "#FFFFFF",
    fontWhite: "#FEFEFE",
    background: "#FBFBFB",
    selectedItem: "#E3FAFF",
  },
  lineHeight: {
    normal: "1rem",
  },
  fontSize: {
    small: "0.875rem",
    normal: "1.125rem",
    large: "1.25rem",
    error: "0.75rem",
  },
  font: {
    poppins: "Poppins, sans-serif",
  },
  iconSize: {
    small: "25",
    normal: "35",
  },
  buttonHeight: {
    normal: "3.125rem",
  },
  buttonWidth: {
    normal: "16rem",
    large: "17.4rem",
  },
};

export enum layers {
  CONTENT = 0,
  ABOVE_CONTENT = 1,
}

export enum screenSizes {
  small = 375,
  medium = 768,
  large = 992,
  extraLarge = 1920,
}
