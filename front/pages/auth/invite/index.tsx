import React from "react";
import { NextPage } from "next";

import InvitationPageContainer from "../../../features/invitation/components/InvitationPageContainer";

const InvitationPage: NextPage = () => <InvitationPageContainer />;

export default InvitationPage;
